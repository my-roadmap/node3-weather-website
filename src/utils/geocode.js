const request = require('postman-request')

const geocode = (address, callback) => {
	const url = `https://api.mapbox.com/geocoding/v5/mapbox.places/${encodeURIComponent(
		address
	)}.json?access_token=pk.eyJ1IjoiYmxlaXdvbGxlIiwiYSI6ImNrODB1aTBhZDAxNXczZW1neXpyZDMxYzEifQ.iuXO9DIyYNO9wfCipNIfuw&limit=1`

	request(url, { json: true }, (error, { body }) => {
		if (error) {
			callback('Unable to connect to location services!', undefined)
		} else if (body.features.length === 0) {
			callback('Unable to find location. Try another search term.', undefined)
		} else {
			const location = body.features[0].place_name
			const [longitude, latitude] = body.features[0].center

			callback(undefined, {
				longitude,
				latitude,
				location
			})
		}
	})
}

module.exports = geocode
