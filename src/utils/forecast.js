const request = require('postman-request')

const forecast = (latitude, longitude, callback) => {
	const url = `https://api.darksky.net/forecast/${process.env.DARK_SKY_API_KEY}/${latitude},${longitude}?units=si`

	request(url, { json: true }, (error, { body }) => {
		if (error) {
			callback('Unable to connect to weather service!', undefined)
		} else if (body.error) {
			callback(body.error, undefined)
		} else {
			const { summary, temperatureMin, temperatureMax } = body.daily.data[0]
			const { temperature, precipProbability } = body.currently

			callback(
				undefined,
				`${summary} It is currently ${temperature} degrees out. 
				There is a ${parseFloat(precipProbability)}% chance of rain.
				Temperatures between ${temperatureMin} and ${temperatureMax} degrees.`
			)
		}
	})
}

module.exports = forecast
