# Nodejs Weather App

A minimalistic weather app

## Demo

Check out the live [demo](https://lauff-weather-application.herokuapp.com/)

## Requirements

- NodeJS >= 10.0
- An API key from [Dark Sky](https://darksky.net/dev). Free tier is sufficient.
- An API key from [MapBox](https://mapbox.com). Free tier is sufficient.

## Getting started

- Install dependencies:

```
npm i
```

- In the root directory, create an dotenv file (`.env`) and write in it:

```
DARK_SKY_API_KEY=YOUR_KEY_HERE
MAP_BOX_API_TOKEN=YOUR_TOKEN_HERE
```

## Development

Start development with

```
npm run dev
```
